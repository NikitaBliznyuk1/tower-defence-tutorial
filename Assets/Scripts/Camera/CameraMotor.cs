﻿using UnityEngine;

namespace Camera
{
	[RequireComponent(typeof(UnityEngine.Camera))]
	public class CameraMotor : MonoBehaviour
	{
		private Transform thisTransform;

		[Header("Settings")]
		[SerializeField]
		private Vector3 offset;

		[SerializeField]
		private bool assignOffsetAutomatically = true;

		[SerializeField]
		private Transform target;

		private readonly float lerpSpeed = 10.0f;

		private void Awake()
		{
			thisTransform = transform;

			if (assignOffsetAutomatically)
				offset = target.position - thisTransform.position;
		}

		private void FixedUpdate()
		{
			Move();
		}

		private void Move()
		{
			thisTransform.position =
				Vector3.Lerp(thisTransform.position, target.position - offset, lerpSpeed * Time.fixedDeltaTime);
		}
	}
}

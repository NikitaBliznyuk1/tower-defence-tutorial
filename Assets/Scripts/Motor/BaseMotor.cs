﻿using System;
using Motor.State;
using UnityEngine;

namespace Motor
{
    public abstract class BaseMotor : MonoBehaviour
    {
        private readonly float baseSpeed = 5.0f;
        private readonly float baseGravity = 25.0f;
    
        protected CharacterController characterController;
        protected Vector3 moveDirection;
        protected Transform thisTransform;
        protected BaseState currentState;

        public float Speed
        {
            get { return baseSpeed; }
        }

        public float Gravity
        {
            get { return baseGravity; }
        }

        protected virtual void Start()
        {
            characterController = gameObject.AddComponent<CharacterController>();
            thisTransform = transform;
        }

        private void FixedUpdate()
        {
            UpdateMotor();
        }

        protected abstract void UpdateMotor();

        protected virtual void Move()
        {
            characterController.Move(moveDirection * Time.fixedDeltaTime);
        }

        protected void ChangeState(string stateName)
        {
            if(currentState != null)
                currentState.Destruct();
        
            Type t = Type.GetType(stateName);
            currentState = gameObject.AddComponent(t) as BaseState;

            if (currentState != null)
                currentState.Construct(this);
        }
    }
}

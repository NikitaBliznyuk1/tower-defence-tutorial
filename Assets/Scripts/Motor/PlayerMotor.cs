﻿using Motor;
using UnityEngine;

public class PlayerMotor : BaseMotor 
{
    protected override void Start()
    {
        base.Start();
        
        ChangeState("Motor.State.WalkingState");
    }

    protected override void UpdateMotor()
    {
        // Get input
        moveDirection = GetInputDirection();
        
        // Filter input
        moveDirection = currentState.ProcessMotion(moveDirection);
        
        // Apply input
        Move();
    }

    private Vector3 GetInputDirection()
    {
        Vector3 inputDirection = Vector3.zero;

        inputDirection.x = Input.GetAxis("Horizontal");
        inputDirection.z = Input.GetAxis("Vertical");

        inputDirection = inputDirection.magnitude > 1 ? inputDirection.normalized : inputDirection;
        
        return inputDirection;
    }
}

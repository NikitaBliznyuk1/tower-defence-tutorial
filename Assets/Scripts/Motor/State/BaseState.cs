﻿using UnityEngine;

namespace Motor.State
{
    public abstract class BaseState : MonoBehaviour
    {
        protected BaseMotor motor;
    
        public virtual void Construct(BaseMotor motor)
        {
            this.motor = motor;
        }

        public virtual void Destruct()
        {
            Destroy(this);
        }

        public virtual void Transition()
        {
        
        }

        public virtual Vector3 ProcessMotion(Vector3 input)
        {
            return input * motor.Speed;
        }

        public virtual Quaternion ProcessRotation(Vector3 input)
        {
            return transform.rotation;
        }
    }
}
